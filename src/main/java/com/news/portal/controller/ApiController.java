package com.news.portal.controller;

import com.news.portal.exceptions.ResourceNotFoundException;
import com.news.portal.model.News;
import com.news.portal.repository.NewsRepository;
import com.news.portal.searching.QuerySearch;
import com.news.portal.searching.SearchService;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
////{"category":"criminal","content":"a typical day in Russia","title":"Wow! Such criminal we've not seen yet!!!"}
@RestController
@RequestMapping("/api")
public class ApiController {
    @Autowired
    NewsRepository newsRepository;

    @Autowired
    SearchService searchService;

    @GetMapping("/news")
    List<News> getAllNews()
    {
        return newsRepository.findAll();
    }

    @PostMapping("/createNews")
    News createNews(@Valid @RequestBody News news)
    {
        return newsRepository.save(news);
    }

    @GetMapping("/news/{id}")
    News getNews(@PathVariable(value = "id") Long id)
    {
        return newsRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("News","id",id));
    }

    @PutMapping("/newsUpdate/{id}")
    News updateNews(@PathVariable(value = "id") Long id,@Valid @RequestBody News newsDatails)
    {
        News news=newsRepository.findById(id)
                .orElseThrow(()->new ResourceNotFoundException("News","id",id));
        news.setContent(newsDatails.getContent());
        news.setCategory(newsDatails.getCategory());

        return newsRepository.save(news);
    }

    @DeleteMapping("deleteNews/{id}")
    ResponseEntity<?> deleteNews(@PathVariable(value = "id") Long id)
    {
        newsRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/searchNews")
    List<News> searchNews(@Valid @RequestBody QuerySearch querySearch)
    {
        return searchService.searchNews(querySearch);
    }
}
