package com.news.portal.searching;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class QuerySearch implements Serializable {
    String query;
    String category;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    @javax.validation.constraints.NotNull
    Date timeCreate;
}
