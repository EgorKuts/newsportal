package com.news.portal.searching.impl;

import com.news.portal.model.News;
import com.news.portal.searching.QuerySearch;
import com.news.portal.searching.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class SearchServiceImp implements SearchService{
    @Autowired
    private final EntityManager entityManager;
    private FullTextEntityManager ftem;

    @Autowired
    public SearchServiceImp(EntityManager entityManager) {
        super();
        this.entityManager = entityManager;
    }

    @Transactional
    @Override
    public List<News> searchNews(QuerySearch querySearch) {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(News.class).get();
        List<News> result=null;
        org.apache.lucene.search.Query resultQuery=qb.range()
                .onField("createdAt")
                .from(querySearch.getTimeCreate())
                .to(new Date()).createQuery();
        log.debug("categorySearch: "+querySearch.getCategory());
        log.debug("qurySearch: "+querySearch.getQuery());
        log.debug("Date: "+ querySearch.getTimeCreate());

        if(querySearch.getQuery()!=null&&querySearch.getQuery().length()>0)
        {
            resultQuery=qb.phrase()
                    .filteredBy(resultQuery)
                    .withSlop(1)
                    .onField("content")
                    .andField("title")
                    .sentence(querySearch.getQuery()+"*")
                    .createQuery();
        }

        if(querySearch.getCategory()!=null&&!querySearch.getCategory().contentEquals("All"))
        {
            resultQuery=qb.keyword()
                    .filteredBy(resultQuery)
                    .onField("category")
                    .matching(querySearch.getCategory())
                    .createQuery();
        }

        result=fullTextEntityManager.createFullTextQuery(resultQuery,News.class).getResultList();
        return result;
    }


    protected FullTextEntityManager getFullTextEntityManager() {
        if (ftem == null) {
            ftem = Search.getFullTextEntityManager(entityManager);
        }
        return ftem;
    }
}
