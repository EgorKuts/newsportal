package com.news.portal.searching;

import com.news.portal.model.News;

import java.util.List;

public interface SearchService {
    List<News> searchNews(QuerySearch query);
}
